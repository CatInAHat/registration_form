<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Java 4 US! Starting Page</title>
    </head>
    <body>
        <h2>Java 4 US! Starting Page</h2>
        <p><a href="${pageContext.request.contextPath}/FormServlet">Fill a form</a></p>
        <p><a href="${pageContext.request.contextPath}/ParticipantServlet">Show all participants</a></p>
    </body>
</html>