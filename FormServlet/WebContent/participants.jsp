<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@ page import="com.domain.Person" %>
	<%@ page import="java.util.ArrayList" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%ArrayList<Person> people = (ArrayList<Person>) request.getAttribute("participants");

    for(Person person : people) {
        out.println(person.getFirstName());
        out.println(person.getSurname());
        out.println(person.getEmail());
        out.println(person.getEmployer());
        out.println(person.getJob()+"<br />");
    }
%>
</body>
</html>