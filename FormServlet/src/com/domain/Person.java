package com.domain;

public class Person {

	private String firstName = "unknown";
	private String surname = "unknown";
	private String email = "unknown";
	private String employer = "unknown";
	private String job = "unknown";
	private String YoB;
	
	public Boolean isValid() {
	    return this.firstName != null &&
	           this.surname != null &&
	           this.email != null &&
	           this.employer != null &&
	           this.job != null;
	}
	
	public Person(String firstName, String surname, String email, String employer, String job) {
        this.firstName = firstName;
        this.surname = surname;
        this.email = email;
        this.employer = employer;
        this.job = job;
    }
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmployer() {
		return employer;
	}
	public void setEmployer(String employer) {
		this.employer = employer;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}

	public String getYoB() {
		return YoB;
	}

	public void setYoB(String yoB) {
		YoB = yoB;
	}

}
