package com.storage;

import java.util.ArrayList;
import java.util.List;

import com.domain.Person;

public class StorageService {

	private static StorageService storageService = null;
	private final Integer maxParticipants = 5;
	
	private ArrayList<Person> db ;
	
	private StorageService() {
        this.db = new ArrayList<>();
    }

    public static StorageService getInstance() {
        if (storageService == null) {
            storageService = new StorageService();
        }

        return storageService;
    }

    public void add(Person person) {
        this.db.add(person);
    }

    public ArrayList<Person> getAll() {
        return this.db;
    }

    public Boolean isFull() {
        return this.db.size() >= this.maxParticipants;
    }

	
	
}
