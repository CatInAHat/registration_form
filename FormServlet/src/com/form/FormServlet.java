package com.form;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.domain.Person;
import com.storage.StorageService;

/**
 * Servlet implementation class FormServlet
 */
@WebServlet(urlPatterns = "/FormServlet")
public class FormServlet extends HttpServlet {
	
	private StorageService storageService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/registrationForm.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        Person person = new Person(
                req.getParameter("name"),
                req.getParameter("surname"),
                req.getParameter("email"),
                req.getParameter("employer"),
                req.getParameter("job")
        );

        if (!person.isValid()) {
            resp.sendRedirect("/registrationForm.jsp");
            return;
        }

        this.storageService.add(person);
        req.getSession().setAttribute("userRegistered", true);

        resp.sendRedirect("/index.jsp");
    }

    @Override
    public void init() throws ServletException {
        this.storageService = StorageService.getInstance();
    }

}
