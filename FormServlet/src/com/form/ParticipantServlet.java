package com.form;

import com.storage.StorageService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/ParticipantServlet")
public class ParticipantServlet extends HttpServlet{
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("participants", StorageService.getInstance().getAll());

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/participants.jsp");
        requestDispatcher.forward(req, resp);
    }
}
