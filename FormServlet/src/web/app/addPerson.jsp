<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Success</title>
</head>
<body>
<jsp:useBean id="person" class="com.domain.Person" scope="session" />

<jsp:setProperty name="person" property="*" /> 

<jsp:useBean id="storage" class="com.storage.StorageService" scope="application" />

<% 
  storage.add(person);
%>

<p>Following registration has been added to storage: </p>
<p>First name: ${person.firstName} </p>
<p>Surname: ${person.firstName} </p>
<p>Email: ${person.firstName} </p>
<p>Employer: ${person.firstName} </p>
<p>Year of birth: <jsp:getProperty name="person" property="yob"></jsp:getProperty></p>
<p>Job: ${person.hobby} </p>
<p>
  <a href="showAllPersons.jsp">Show all persons</a>
</p>
</body>
</html>